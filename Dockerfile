FROM node:current-alpine
ARG FIREBASE_VERSION=latest
RUN npm install -g firebase-tools@$FIREBASE_VERSION
RUN apk add jq
